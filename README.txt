-- SUMMARY --

A module which allows for limiting items per menu to a certain amount.

-- REQUIREMENTS --

Drupal 7:
The Menu module needs to be enabled.

Drupal 8:
The Menu UI module needs to be enabled.

-- INSTALLATION --

* Install as usual, see
  http://drupal.org/documentation/install/modules-themes/modules-7 for further
  information.

-- CONFIGURATION --

After enabling the module access the "Edit Menu" tab for the menu you want to
limit. You can add an item limitation in the textfield "Item Limitation".
Per Default, 0 is prefilled which means there is no limitation.

-- CONTACT --

Current maintainers:

Lucio Waßill --> https://drupal.org/user/397618

This project has been sponsored by:
* undpaul
  Drupal experts providing professional Drupal development services.
  Visit http://www.undpaul.de for more information.
